<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>About Us</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">About Us</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix"> <div class="clearfix"><img src="img/IMG_3784.jpg" alt="" class="img-align-right">
    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. </p></div>
    <hr/>
    <h3 class="sectionTitle">OUR ROOM FACILITIES</h3>
    <div class="facilities_list">
      <div class="row clearfix">
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <div class="single_facilities_name clearul"> <img src="img/home-facilities-icon-two.png" alt="">
            <p>Air conditioning</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <div class="single_facilities_name clearul"> <img src="img/home-facilities-icon-one.png" alt="">
            <p>Breakfast</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <div class="single_facilities_name clearul"> <img src="img/home-facilities-icon-eight.png" alt="">
            <p>Free Parking</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <div class="single_facilities_name clearul"> <img src="img/home-facilities-icon-ten.png" alt="">
            <p>GYM Facility</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <div class="single_facilities_name clearul"> <img src="img/home-facilities-icon-three.png" alt="">
            <p>Pet allowed</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <div class="single_facilities_name clearul"> <img src="img/home-facilities-icon-five.png" alt="">
            <p>TV LCD</p>
          </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
          <div class="single_facilities_name clearul"> <img src="img/home-facilities-icon-twelve.png" alt="">
            <p>Wi-fi service</p>
          </div>
        </div>
      </div>
    </div>
    <hr/>
    <div class="bottomContent"><img src="img/IMG_3817.jpg" alt="" class="img-align-left">
      <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? </p>
    </div>
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php include('footer.php')?>