<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Contact Us</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Contact Us</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix">
    
    <div class="contact-us">
					
						<div class="row">
							<div class="col-md-8">
							
								<!-- Contact map -->
								<div class="contact-map">
									<!-- Map Link -->
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3515.442629469128!2d83.94430081445647!3d28.2242420091942!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x399594c4cad93e37%3A0xaee59730680815e0!2sGusto+The+Restaurant!5e0!3m2!1sen!2snp!4v1511086595205" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								</div>
								<hr />
								
								<!-- Contact Form -->
								<div class="contact-form">
									<h5>Contact Form</h5>
									<!-- Form -->
									<form class="form" role="form">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group">
													<input type="text" class="form-control" placeholder="Enter Name">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<input type="text" class="form-control" placeholder="Enter Email">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group">
													<input type="text" class="form-control" placeholder="Enter Phone">
												</div>
											</div>
										</div>										
										<div class="form-group">
											<textarea class="form-control" id="comments" rows="8" placeholder="Enter Message"></textarea>
										</div>
										<!-- Button -->
										<button type="button" class="btn btn-outline-danger">Submit</button>
										
									</form>
								</div>
								
								<br />
								
							</div>
							<div class="col-md-4 col-sm-6">
								
								<div class="well">
									<h6><i class="fa fa-user"></i>Get In Touch</h6>
									<p>Please Contact us for Domain Name Registration, Web Hosting, Website Designing, Web Development, Web Programming, Website Designing & Development Training, Social Media Marketing & Bulk SMS.</p>
									<div class="brand-bg">
										<!-- Social Media Icons -->
										<a href="https://www.facebook.com/ranashotel" class="facebook"><i class="fa fa-facebook circle"></i></a>
										<a href="#" class="twitter"><i class="fa fa-twitter circle"></i></a>
										<a href="#" class="google-plus"><i class="fa fa-google-plus circle"></i></a>
										<a href="#" class="linkedin"><i class="fa fa-linkedin circle"></i></a>
										<a href="#" class="pinterest"><i class="fa fa-pinterest circle"></i></a>
									</div>
								</div>
								
								<div class="well">
									<!-- Heading -->
									<h6>Gusto Accommodation Pvt. Ltd.</h6>
									<!-- Paragraph -->
									<p>
										Lakeside, Pokhara 6, Kaski, Nepal</p>
									<p> <i class="fa fa-phone"></i><b>Telephone :</b>+977 (61) 061-462320<br />
									
									<i class="fa fa-mobile"></i><b>Mobile :</b> +977-9810225152<br/>
                                    <i class="fa fa-envelope"></i><b>Mail :</b> <a href="#">: info@gustoaccommodation.com</a><br />
                                    <i class="fa fa-globe"></i><b>Website :</b> <a href="#">www.gustoaccommodation.com</a>
                                    </p>
								</div>
								
								
								
							</div>
						</div>
												
					</div>
  </div>
</section>
<!--Hero Section End--> 


<?php include('footer.php')?>