<!--Footer Start-->

<footer id="page-footer" class="textured-gradient ">
  <div class="container">
    <div class="row clearfix">
      <div class="col-lg-4">
        <h6> Gusto Accomodation</h6>
        <ul>
          <li> <a href="#">About Us</a> </li>
          <li> <a href="#">Gifts</a> </li>
          <li> <a href="#">Careers</a> </li>
          <li> <a href="#">Contact Us</a> </li>
        </ul>
      </div>
      <div class="col-lg-4">
        <h6>Media</h6>
        <ul>
          <li> <a href="#" target="_blank">Newsroom</a> </li>
          <li> <a href="#">Press Contacts</a> </li>
          <li> <a href="#" target="_blank">Download Brochure</a> </li>
        </ul>
      </div>
      <div class="col-lg-4">
        <form action="#" method="POST" class="newsletter">
          <h6 id="newsletter_signup" tabindex="0">Newsletter Signup</h6>
          <small>Enter your email address below to receive our monthly fun-filled newsletter.</small>
          <div class="form-group">
            <input type="text" name="newsletter-email" class="form-control" id="email_inputs" placeholder="Email address" aria-invalid="false" aria-labelledby="newsletter_signup" aria-describedby="err_email" aria-required="true">
          </div>
          <button class="btn btn-outline-warning" role="button">Submit</button>
        </form>
      </div>
    </div>
    <div class="bottom_footer text-center"> <small>© 2017 Gusto Accomodation. All Rights Reserved.</small> </div>
  </div>
</footer>
<!--Footer End--> 
<!-- Optional JavaScript --> 
<!-- jQuery first, then Popper.js, then Bootstrap JS --> 
<script src="js/jquery.min.js" type="text/javascript"></script> 
<script src="js/popper.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript" ></script> 
<script type="text/javascript" src="js/modernizr.js"></script> 
<script type="text/javascript" src="js/jquery.appear.js"></script>
<!-- Include the plugin's CSS and JS: --> 
<script type="text/javascript" src="js/jquery.flexslider.js"></script>
<script src="js/owl.carousel.js" type="text/javascript" ></script> 
<script src="js/scrollPosStyler.js" type="text/javascript" ></script> 
<!-- Include the plugin's CSS and JS: --> 
<script type="text/javascript" src="js/bootstrap-multiselect.js"></script> 
<script type="text/javascript" src="js/jquery-ui.js"></script> 
<script type="text/javascript" src="js/moment.min.js"></script> 
<script type="text/javascript" src="js/jquery.daterangepicker.min.js"></script> 
<!-- Magnific Popup javascript --> 
<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script> 
<!-- Isotope javascript --> 
<script type="text/javascript" src="js/isotope.pkgd.min.js"></script> 
<script src="js/script.js" type="text/javascript" ></script> 

</body></html>