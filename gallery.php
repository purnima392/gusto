<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Gallery</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Gallery</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container clearfix">
    
    <div class="masonry-grid row">
								
								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3758.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3758.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3777.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3777.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3784.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3784.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3790.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3790.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3822.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3822.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->
                                <!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3783.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3783.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3823.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3823.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3784.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3784.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->
<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3758.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3758.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3777.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3777.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3784.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3784.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3790.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3790.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->

								<!-- masonry grid item start -->
								<div class="masonry-grid-item col-sm-6 col-md-4">
									<!-- blogpost start -->
									<article class="clearfix blogpost">
										<div class="overlay-container">
											<img src="img/IMG_3822.jpg" alt="">
											<div class="overlay">
												<div class="overlay-links">
													
													<a href="img/IMG_3822.jpg" class="popup-img" title="image title"><i class="fa fa-search-plus"></i></a>
												</div>
											</div>
										</div>
										
										
									</article>
									<!-- blogpost end -->
								</div>
								<!-- masonry grid item end -->
								
							

							</div>
  </div>
</section>
<!--Hero Section End--> 



<?php include('footer.php')?>