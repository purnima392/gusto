<!doctype html>
<html lang="en">
<head>
<title>Gusto Accomodation</title>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Bootstrap CSS -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<!--Fonts-->
<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Cookie" rel="stylesheet">

<!--Icon Fonts-->
<link rel="stylesheet" type="text/css" href="css/flaticon.css">
<link href="fonts/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="css/animations.css" rel="stylesheet">
<link rel="stylesheet" href="css/flexslider.css">
<link rel="stylesheet" href="css/owl.carousel.min.css">
<link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>
<link rel="stylesheet" href="css/jquery-ui.css" type="text/css"/>
<link rel="stylesheet" href="css/daterangepicker.min.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/style.css">
</head>
<body>
	<!-- scrollToTop --> 
<!-- ================ -->
<div class="scrollToTop"><i class="fa fa-angle-up"></i></div>
<!--Header Start-->
<header id="header"> <a href="#" class="switchMenu" style="display:none"><i class="fa fa-list"></i></a>
  <div class="innerHeader">
    <div class="row no-gutters">
      <div class="col-lg-4">
        <ul class="top_social_links">
          <li><a href="#" class="facebook" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#" class="twitter" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#" class="instagram" data-toggle="tooltip" data-placement="top" title="Instagram"><i class="fa fa-instagram"></i></a></li>
        </ul>
      </div>
      <div class="col-lg-4">
        <h1 class="logo"><a href="index.php">Gusto Accomodation</a></h1>
      </div>
      <div class="col-lg-4 topContact">
        <h3><i class="flaticon-telephone"></i><a href="#">977-9810225152</a></h3>
      </div>
    </div>
  </div>
</header>
<nav id="NavMenu" class="navbar navbar-expand-lg navbar-light sps sps--abv"> <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav">
        <li class="nav-item overview">Overview</li>
        <li class="nav-item active"> <a class="nav-link" href="index.php">Home <span class="sr-only">(current)</span></a> </li>
        <li class="nav-item"> <a class="nav-link" href="about.php">About Us</a> </li>
        
        <li class="nav-item"> <a class="nav-link" href="traiff.php">Tariff</a> </li>
        <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> Room & Rate </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown"> <a class="dropdown-item" href="room&rate.php">All Room</a><a class="dropdown-item" href="#">Mercury</a> <a class="dropdown-item" href="#">Venus</a> <a class="dropdown-item" href="#">Jupiter</a> <a class="dropdown-item" href="#">Mars</a><a class="dropdown-item" href="#">Saturn</a><a class="dropdown-item" href="#">Earth</a></div>
        </li>
        <li class="nav-item"> <a class="nav-link" href="gallery.php">Gallery</a> </li>
        <li class="nav-item"> <a class="nav-link" href="testimonial.php">Testimonial</a> </li>
        <li class="nav-item"> <a class="nav-link" href="contact.php">Contact Us</a> </li>
      </ul>
    </div>
  </nav>
<!--Header End--> 