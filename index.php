<?php include('header.php')?>
<!--Hero Section Start-->
<section class="heroSection">
  <div class="sliderBanner">
    <div class="flexslider">
          <ul class="slides">
             <li> <img src="img/IMG_3815.jpg" alt="Slider">
          <div class="meta">
            <h1>A Brand New Hotel <span>Beyond Ordinary</span></h1>
            <h2>This is a perfect villa with spa center and hot tub for private, family and corporate rest in Le Marche region in Italy, with best nature views.</h2>
            <a href="#" class="btn btn-warning">Discover the full story</a> 
            </div>
        </li>
        <li> <img src="img/IMG_3822.jpg" alt="Slider">
          <div class="meta">
            <h1>A Brand New Hotel <span>Beyond Ordinary</span></h1>
            <h2>This is a perfect villa with spa center and hot tub for private, family and corporate rest in Le Marche region in Italy, with best nature views.</h2>
            <a href="#" class="btn btn-warning">Discover the full story</a> 
            </div>
        </li>
  	    		
          </ul>
        </div>
  </div>
  </div>
  </div>
</section>
<!--Hero Section End--> 
<!--Booking Section Start-->
<section class="bookingSection">
  <div class="row filter_room no-gutters">
    <div class="col-lg-2">
      <h2 class="bookRoom">Book Your <span>Rooms</span></h2>
    </div>
    <div class="col-lg-9">
      <div class="row">
        <div class="col-lg-3">
          <h4>Room Category</h4>
          <!-- Build your select: -->
          <select id="roomCategory" multiple="multiple">
            <option value="mercury">Mercury</option>
            <option value="venus">Venus</option>
            <option value="jupiter">Jupiter</option>
			<option value="mars">Mars</option>
			<option value="saturn">Saturn</option>
			<option value="earth">Earth</option>
			
          </select>
        </div>
        <div class="col-lg-2">
          <h4>Adults</h4>
          <!-- Build your select: -->
          <select id="adults" >
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div>
        <div class="col-lg-2">
          <h4>Children</h4>
          <!-- Build your select: -->
          <select id="children">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
            <option value="6">6</option>
          </select>
        </div>
        <div class="col-lg-5"> <span id="two-inputs"><span class="input-wrap">
          <h4>Check In</h4>
          <input id="date-range200" class="form-control" size="20" value="">
          </span><span class="input-wrap">
          <h4>Check Out</h4>
          <input id="date-range201" class="form-control" size="20" value="">
          </span></span> </div>
      </div>
    </div>
    <div class="col-lg-1">
      <div class="btn-wrapper">
        <button type="submit" class="btn btn-dark">Filter</button>
      </div>
    </div>
  </div>
</section>
<!--Booking Section End--> 
<!--About Section Start-->
<section class="aboutSection">
  <div class="container">
    <div class="row">
      <div class="col-lg-6"> <img src="img/IMG_3790.jpg" alt=""> </div>
      <div class="col-lg-6">
        <div class="introBlock">
          <div class="introContent">
            <h3><span>Welcome To</span> Gusto Accomodation</h3>
            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</p>
            <a href="#" class="btn btn-outline-warning">Read More</a> </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--About Section End--> 
<!--Service Section Start-->
<section class="serviceSection graySection">
  <div class="container">
    <div class="title text-center">
      <h2>Our Services</h2>
      <p> <span>Hotel provides all services you need.</span></p>
    </div>
    <div class="serviceList clearfix">
      <ul>
        <li class="clearfix">
          <div class="serviceInfo"><i class="icon"><img src="img/room-service.svg" alt="roomservice"></i>
            <div class="serviceContent">
              <h5>Room Service</h5>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
            </div>
          </div>
        </li>
        <li class="clearfix">
          <div class="serviceInfo"><i class="icon"><img src="img/ticket.svg" alt="roomservice"></i>
            <div class="serviceContent">
              <h5>Ticketing</h5>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
            </div>
          </div>
        </li>
        <li class="clearfix">
          <div class="serviceInfo"><i class="icon"><img src="img/restaurant-cutlery-circular-symbol-of-a-spoon-and-a-fork-in-a-circle.svg" alt="roomservice"></i>
            <div class="serviceContent">
              <h5>Resturant</h5>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
            </div>
          </div>
        </li>
        <li class="clearfix">
          <div class="serviceInfo"><i class="icon"><img src="img/wifi.svg" alt="roomservice"></i>
            <div class="serviceContent">
              <h5>Wifi</h5>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
            </div>
          </div>
        </li>
        <li class="clearfix">
          <div class="serviceInfo"><i class="icon"><img src="img/laundry.svg" alt="roomservice"></i>
            <div class="serviceContent">
              <h5>Laundry</h5>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam</p>
            </div>
          </div>
        </li>
      </ul>
    </div>
  </div>
</section>

<!--Room & Rates Start-->
<section class="roomRate">
  <div class="container">
    <div class="title text-center">
      <h2>Room & Rate</h2>
      <p> <span>Find the room for your unforgettable stay</span></p>
    </div>
    <div class="service_block">
      <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item"> <a class="nav-link active" href="#mercury" role="tab" data-toggle="tab">Mercury</a> </li>
        <li class="nav-item"> <a class="nav-link" href="#venus" role="tab" data-toggle="tab">Venus</a> </li>
		<li class="nav-item"> <a class="nav-link" href="#jupiter" role="tab" data-toggle="tab">Jupiter</a> </li>
		<li class="nav-item"> <a class="nav-link" href="#mars" role="tab" data-toggle="tab">Mars</a> </li>
		<li class="nav-item"> <a class="nav-link" href="#saturn" role="tab" data-toggle="tab">Saturn</a> </li>
		
        <li class="nav-item"> <a class="nav-link" href="#earth" role="tab" data-toggle="tab">Earth</a> </li>
      </ul>
      
      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane slide in active" id="mercury">
          <div class="row">
            <div class="col-lg-6"> <img src="img/IMG_3807.jpg" alt=""> </div>
            <div class="col-lg-6">
              <div class="roomTitle clearfix">
                <h3>Mercury</h3>
                <a href="#" class="btn btn-outline-warning">Book Now</a> </div>
              <p>Spacious Deluxe Rooms offer refined contemporary accommodation. With a relaxing blend of natural tones and rich comfortable furnishings, these rooms are perfect for business or leisure. Equipped with a desk and complimentary internet connectivity, these rooms retain a modern residential ambience with high ceilings and large picture windows.</p>
              <b>Amenities</b>
              <ul>
                <li>Unlimited Tea & Coffee</li>
                <li>Mineral Water</li>
                <li>HD TV</li>
				<li>High Speed Internet</li>
				
              </ul>
              <p class="price">Price start at: <b>$30</b> for night</p>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="venus">
          <div class="row">
            <div class="col-lg-6"> <img src="img/IMG_3781.jpg" alt=""> </div>
            <div class="col-lg-6">
              <div class="roomTitle clearfix">
                <h3>Venus</h3>
                <a href="#" class="btn btn-outline-warning">Book Now</a> </div>
              <p>Spacious Deluxe Rooms offer refined contemporary accommodation. With a relaxing blend of natural tones and rich comfortable furnishings, these rooms are perfect for business or leisure. Equipped with a desk and complimentary internet connectivity, these rooms retain a modern residential ambience with high ceilings and large picture windows.</p>
              <b>Amenities</b>
              <ul>
                <li>Unlimited Tea & Coffee</li>
                <li>Mineral Water</li>
                <li>HD TV</li>
				<li>High Speed Internet</li>
              </ul>
              <p class="price">Price start at: <b>$30</b> for night</p>
            </div>
          </div>
        </div>
        <div role="tabpanel" class="tab-pane fade" id="jupiter">
          <div class="row">
            <div class="col-lg-6"> <img src="img/IMG_3758.jpg" alt=""> </div>
            <div class="col-lg-6">
              <div class="roomTitle clearfix">
                <h3>Jupiter</h3>
                <a href="#" class="btn btn-outline-warning">Book Now</a> </div>
              <p>Spacious Deluxe Rooms offer refined contemporary accommodation. With a relaxing blend of natural tones and rich comfortable furnishings, these rooms are perfect for business or leisure. Equipped with a desk and complimentary internet connectivity, these rooms retain a modern residential ambience with high ceilings and large picture windows.</p>
              <b>Amenities</b>
              <ul>
                <li>Unlimited Tea & Coffee</li>
                <li>Mineral Water</li>
                <li>HD TV</li>
				<li>High Speed Internet</li>
              </ul>
              <p class="price">Price start at: <b>$30</b> for night</p>
            </div>
          </div>
        </div>
		<div role="tabpanel" class="tab-pane fade" id="mars">
          <div class="row">
            <div class="col-lg-6"> <img src="img/IMG_3783.jpg" alt=""> </div>
            <div class="col-lg-6">
              <div class="roomTitle clearfix">
                <h3>Mars</h3>
                <a href="#" class="btn btn-outline-warning">Book Now</a> </div>
              <p>Spacious Deluxe Rooms offer refined contemporary accommodation. With a relaxing blend of natural tones and rich comfortable furnishings, these rooms are perfect for business or leisure. Equipped with a desk and complimentary internet connectivity, these rooms retain a modern residential ambience with high ceilings and large picture windows.</p>
              <b>Amenities</b>
              <ul>
                <li>Unlimited Tea & Coffee</li>
                <li>Mineral Water</li>
                <li>HD TV</li>
				<li>High Speed Internet</li>
              </ul>
              <p class="price">Price start at: <b>$30</b> for night</p>
            </div>
          </div>
        </div>
		<div role="tabpanel" class="tab-pane fade" id="saturn">
          <div class="row">
            <div class="col-lg-6"> <img src="img/IMG_3777.jpg" alt=""> </div>
            <div class="col-lg-6">
              <div class="roomTitle clearfix">
                <h3>Saturn</h3>
                <a href="#" class="btn btn-outline-warning">Book Now</a> </div>
              <p>Spacious Deluxe Rooms offer refined contemporary accommodation. With a relaxing blend of natural tones and rich comfortable furnishings, these rooms are perfect for business or leisure. Equipped with a desk and complimentary internet connectivity, these rooms retain a modern residential ambience with high ceilings and large picture windows.</p>
              <b>Amenities</b>
              <ul>
                <li>Mini Bar</li>
                <li>Top Bath</li>
                <li>Balcony</li>
				<li>Full Lake View</li>
              </ul>
              <p class="price">Price start at: <b>$55</b> for night</p>
            </div>
          </div>
        </div>
		<div role="tabpanel" class="tab-pane fade" id="earth">
          <div class="row">
            <div class="col-lg-6"> <img src="img/IMG_3817.jpg" alt=""> </div>
            <div class="col-lg-6">
              <div class="roomTitle clearfix">
                <h3>Earth</h3>
                <a href="#" class="btn btn-outline-warning">Book Now</a> </div>
              <p>Spacious Deluxe Rooms offer refined contemporary accommodation. With a relaxing blend of natural tones and rich comfortable furnishings, these rooms are perfect for business or leisure. Equipped with a desk and complimentary internet connectivity, these rooms retain a modern residential ambience with high ceilings and large picture windows.</p>
              <b>Amenities</b>
              <ul>
                <li>Mini Bar</li>
                <li>Top Bath</li>
                <li>Balcony</li>
				<li>Full Lake View</li>
              </ul>
              <p class="price">Price start at: <b>$65</b> for night</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Room & Rates End--> 
<!--Testimonial Start-->
<section class="testimonial-wrapper parallax">
  <div class="container">
    <div class="title text-center whiteText">
      <h2>Testimonal</h2>
    </div>
    <div class="row">
      <div class="col-lg-8 offset-lg-2">
        <div class="owl-carousel owl-theme testimonial">
          <div class="item">
            <div class="testimonialInfo">
              <h4>You have a great team of masseurs</h4>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
              <div class="testimonailDetail">
                <div class="testimonialImg"></div>
                <span class="name">Purnima Sai</span> <span class="post">Web Designer</span> </div>
            </div>
          </div>
          <div class="item">
            <div class="testimonialInfo">
              <h4>You have a great team of masseurs</h4>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
              <div class="testimonailDetail">
                <div class="testimonialImg"></div>
                <span class="name">Purnima Sai</span> <span class="post">Web Designer</span> </div>
            </div>
          </div>
          <div class="item">
            <div class="testimonialInfo">
              <h4>You have a great team of masseurs</h4>
              <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
              <div class="testimonailDetail">
                <div class="testimonialImg"></div>
                <span class="name">Purnima Sai</span> <span class="post">Web Designer</span> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!--Testimonial End-->
<div class="top-footer">
  <div class="container custom-container clearfix">
    <h3 class="float-left">Follow us through social medai</h3>
    <ul class="float-right social_media">
      <li><a href="https://www.facebook.com/ranashotel"><i class="fa fa-facebook"></i><span><b>3,060</b>Followers</span></a></li>
      <li><a href="#"><i class="fa fa-twitter"></i><span><b>1,060</b>Followers</span></a></li>
      <li><a href="#"><i class="fa fa-tripadvisor"></i><span><b>2,060</b>Followers</span></a></li>
    </ul>
  </div>
</div>
<?php include('footer.php')?>