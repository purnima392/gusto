<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Room & Rates</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Room & Rates</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container">
  	<div class="row">
    	<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/IMG_3817.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Mercury</a></h3>
					<span class="price">$30.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/IMG_3822.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Venus</a></h3>
					<span class="price">$30.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/IMG_3807.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Jupiter</a></h3>
					<span class="price">$30.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/IMG_3815.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Mars</a></h3>
					<span class="price">$30.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/IMG_3758.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Saturn</a></h3>
					<span class="price">$55.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
		<div class="col-lg-4">
        	<div class="roomList">
            	<div class="roomImg"><a href="#"><img src="img/IMG_3777.jpg" alt=""></a></div>
                <div class="roomInfo">
					<div class="roomTitle clearfix">
                	<h3><a href="#">Earth</a></h3>
					<span class="price">$65.00/<sub>Night</sub></span>
					</div>
                    <p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. </p>
                    <a href="#" class="btn btn-outline-warning">Book Now</a>
                </div>
            </div>
        </div>
    </div>
  </div>
</section>
<!--Hero Section End--> 


<?php include('footer.php')?>