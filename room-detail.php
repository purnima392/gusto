<?php include('header.php')?>
<!--Pagetop Section Start-->
<section class="pagetop parallax">
  <div class="container">
    <div class="pageTitle">
      <h3>Room Detail</h3>
      <nav aria-label="breadcrumb" role="navigation">
        <ol class="breadcrumb">
          <li class="breadcrumb-item"><a href="index.php">Home</a></li>
          <li class="breadcrumb-item active" aria-current="page">Room Detail</li>
        </ol>
      </nav>
    </div>
  </div>
</section>
<section class="inner_content">
  <div class="container">
    <div class="roomDetail">
    <div class="row">
    	<div class="col-lg-6">
        	<img src="img/pexels-photo-164595-840x560.jpg" alt="">
            
        </div>
        <div class="col-lg-6">
        	<div class="detail_title"><h3>Deluxe Room</h3>
        <p class="lead">A perfectly equipped room with breathtaking ocean views</p>
        	<h5>Prices start at: <b>$4,060</b> for 35 nights</h5>
        </div>
        <p>Deluxe Rooms in our hotel are perfectly equipped for traveling couples or friends. The park or olive grove view will bring you unforgettable memories.</p>
        <p>These rooms designed with open-concept living area are very bright and spacious – they are available with either double or queen beds. Room sizes are different – from 30 to 35 sqm. The interior is made with a warm palette tons of walls and furniture. A work desk and stationary set are at your disposal. Makeup mirror and chair are also included.</p>
        <hr/>
        <h5>Amenities</h5>
        <ul class="list">
                <li>Breakfast</li>
                <li>Complimentary Wifi</li>
                <li>Complimentary local calls and long-distance calls</li>
              </ul>
        <div class="btn-wrapper"><a href="#" class="btn btn-outline-warning">Book Now</a></div>
        </div>
    </div>
    	
    </div>
  </div>
</section>
<!--Hero Section End--> 

<!--Service Section Start--> 

<!--Room & Rates Start--> 

<!--Room & Rates End--> 
<!--Testimonial Start--> 

<!--Testimonial End-->

<?php include('footer.php')?>